FROM alpine

WORKDIR /app

ADD ./requirements.txt .
ADD entrypoint.sh .

RUN set -x \
    # Set permissions
    && chmod +x entrypoint.sh \
    # Install dependencies
    && apk add py3-virtualenv \
    && virtualenv -p python3 venv \
    && . venv/bin/activate \
    && pip install -r ./requirements.txt

ADD ./src/* .

ENV FLASK_APP=main.py

CMD ["/app/entrypoint.sh"]
