IMAGE_TAG=app-example

build:
	docker build -t $(IMAGE_TAG) .

run:
	docker run -it --rm -p 8080:80 $(IMAGE_TAG)

build-run: build run
