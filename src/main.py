from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/contact')
def contact():
    return 'Oi, só mandar msg no Telegram'
